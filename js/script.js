(function ($) {
	$(document).ready(function() {


		function lpHeader () {
			if ($(window).scrollTop() == 0 ) {
				$('header.down').removeClass('down');
				console.log($(window).width())

			} else{
				$('header').addClass('down');
			}
		}
		lpHeader();
		$(window).on('scroll',lpHeader);

		var lpMenuItems=$('.links ul.nav li a');
		lpMenuItems.each(function() {
			var link = $(this),
				linkHref = link.attr('href'),
				linkTrgt = $(linkHref);
			if (linkTrgt.length > 0) {
				link.on('click', function(event) {
					event.preventDefault();
					var offset = linkTrgt.offset().top;
					var offset = linkTrgt.offset().top;
					$('html,body').animate({scrollTop:offset-90}, 1000);
				
				});
			}
		});

		$('.glyphicon-menu-hamburger').on('click', function(event) {
			var hamPosition = $('.hammenu').css('left');
			if (hamPosition=='0px') {
				$('.hammenu').css({
					left: '-126px'
				});
			} else {
				$('.hammenu').css({
					left: 0
				});
			}

			
		});
		$('.sidemenu li a').on('click', function(event) {
			$('.hammenu').css({
				left: '-126px'
			});
		});


	});
})(jQuery);

	var map;
	var coord={lat: 53.901325, lng: 27.559750};
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				center: coord,
				zoom: 17,
				scrollwheel: false,
				draggable: false 
			});
			var marker = new google.maps.Marker({
          		position: coord,
         		map: map
        });
		}